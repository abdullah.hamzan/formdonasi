
import React from 'react'
import FormDonasiComponent from '../components/FormDonasiComponent'
import NavbarComponent from '../components/NavbarComponent'

import '../styles/Home.css'

const Home = () => {
    return (
        <div>
         <NavbarComponent />   
         <div className='container'>
        <FormDonasiComponent />
        </div>
            
        </div>
    )
}

export default Home
