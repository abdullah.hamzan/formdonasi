
import React, { useEffect, useState } from 'react'
import { Donasi } from '../assets/image'
import { TextField } from '@material-ui/core';
import {Box} from '@material-ui/core';
import { Button } from '@material-ui/core';

import '../styles/Form.css'
import Gap from './Gap';
import axios from 'axios';
import NumberFormat from 'react-number-format';

const FormDonasiComponent=()=> {

    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const [donasi,setDonasi]=useState('')
    const [error_inputEmail,setErrorInputEmail]=useState(false)
    const [error_inputPassword,setErrorInputPassword]=useState(false)
    const [error_inputDonasi,setErrorInputDonasi]=useState(false)
    const [quote,setQuoet]=useState([])

    console.log("email:",email)


    useEffect(()=>{

        getDataApi()



    },[])

    const getDataApi= async()=>{

        try {

            const qod= await axios.get('https://quotes.rest/qod', {
                headers:{
                    'Content-Type':'application/json'
                }
            });

            console.log("qod:",qod.data.contents.quotes)
            const quotes=qod.data.contents.quotes;
            setQuoet(quotes)
            
        } catch (error) {

            console.log("error:",error)
            
        }


    }

    console.log("quotes:",quote)
    
    return (
        <div style={{ display:'flex', justifyContent:'space-between', marginLeft:'20px', marginRight:'20px' }}>
        <div style={{ width:'550px', height:'580px', borderRadius:'20px', backgroundColor:'white', marginTop:'50px' }}>
          <h3 style={{  marginLeft:'20px' , marginBottom: '20px'}}>Mari Donasi</h3>
          <div style={{ marginLeft:'40px' }}>
         
         <Box
            component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '420px' },
                    }}
                    noValidate
                    autoComplete="off"
                    
            >
            <TextField type='email'  
            label="Email" variant="outlined" value={email} 

            onFocus={()=>{

                setErrorInputEmail(false)

            }}
            onBlur={
                ()=>{
                    if (email===''){

                        setErrorInputEmail(true)
                    }else {

                        var atps=email.indexOf("@");
                        var dots=email.lastIndexOf(".");
                if (atps<1 || dots<atps+2 || dots+2>=email.length) {
                    setErrorInputEmail(true)

                       
                    } else {
                        setErrorInputEmail(false)
                     }

                    }

               
                }

            }

            onChange={(e)=>{
        
            setEmail(e.target.value)

          


        }

      
            
            } />
            </Box>
            {
                error_inputEmail && <span style={{ color:'red'  }}>Email tidak Boleh kosong</span>
            }
            
            <Gap height={10} />
            <Box
            component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '420px' },
                    }}
                    noValidate
                    autoComplete="off"
            >
            <TextField type='password'  label="Password" variant="outlined" 
            value={password} onChange={(e)=>setPassword(e.target.value)} 

            onFocus={()=>{

                setErrorInputPassword(false)

            }}
            onBlur={
                ()=>{
                    if (password===''){
                        setErrorInputPassword(true)
                    }else {

                        if (password.length <6){

                            setErrorInputPassword(true)

                        }


                    }
                }

            }

           
    
            
            
            />
            </Box>
            {
                error_inputPassword && <span style={{ color:'red'  }}>Password tidak Boleh kosong</span>
            }
            <Gap height={10} />
            <Box
            component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '410px',height: '40px' },
                    }}
                    noValidate
                    autoComplete="off"
            >
                
            {/* <TextField type='number'
            
            value={donasi} 
            label="Donasi" 
            variant="outlined" 
            onChange={(e)=>setDonasi(e.target.value)}  

            onFocus={()=>{

                setErrorInputDonasi(false)

            }}
            onBlur={
                ()=>{
                    if (donasi===''){
                        setErrorInputDonasi(true)
                    }
                }

            }

    
            
            
            /> */}

{/* <TextField type='number'
            
            value={donasi} 
            
            label="Donasi" 
            variant="outlined" 
            onChange={(e)=>setDonasi(e.target.value)}  

            onFocus={()=>{

                setErrorInputDonasi(false)

            }}
            onBlur={
                ()=>{
                    if (donasi===''){
                        setErrorInputDonasi(true)
                    }
                }

            }

            
            /> */}

     <NumberFormat 
     placeholder='Donasi'
     style={{ paddingLeft:'10px' }}
     thousandSeparator={true} 
     prefix={'Rp'} 
     onFocus={()=>{

        setErrorInputDonasi(false)

    }}
     value={donasi} 
     onChange={(e)=>setDonasi(e.target.value)}

     onBlur={
        ()=>{
            if (donasi===''){
                setErrorInputDonasi(true)
            }
        }

    }
     
     />
           
            </Box>
            
            {error_inputDonasi && <span style={{ color:'red'  }}>Donasi tidak Boleh kosong</span>}
            <Gap height={10} />
            <Box
            component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '420px' },
                    }}
                    noValidate
                    autoComplete="off"
            >
                <Button variant="contained" color="primary" onSubmit={()=>{

                    console.log("Oke saja");

                }} >SUBMIT</Button>
                </Box>
                <h3 style={{ textAlign:'center' , marginLeft: '-50px'}}>Quotes of the day:</h3>
                {
                    quote.map(item=>(

                <h4 style={{ textAlign:'center' , marginLeft: '-50px',color:'blue'}}>{item.quote}</h4>


                    ))
                }
            </div>

        </div>
   
          <img src={Donasi} style={{ width:'580px', width:'580px', borderRadius:'20px',marginTop:'50px'  }}  />
      
         
        </div>
    )
}

export default FormDonasiComponent
