
import React from 'react'
import { Logo } from '../assets/logo'
import '../styles/Navbar.css'


const NavbarComponent = () => {
    return (
        <div className='nav'>
            <img src={Logo}  />
            <h1>Donasi</h1>
            
        </div>
    )
}

export default NavbarComponent
